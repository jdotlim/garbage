## Assertions

#' Test assertions
#'
#' Test a set of one or more assertions for truthfulness.
#'
#' This function is a useful tool for defensive programming as any assertion
#' that does not evaluate to `TRUE` will result in an error. It is also useful
#' to make assertions with custom error messages. In addition to being able to
#' pass an error message directly to this function, functions that return
#' logical values can provide a way to generate custom error messages. This is
#' done by adding a "fail" attribute to the function. The "fail" attribute
#' should be a function of the form `function(call, env)` and should return a
#' character string. The `call` will be the call to the function and `env` will
#' be the evaluation environment. The assert functionally was heavily influenced
#' by the \href{https://github.com/hadley/assertthat}{assertthat} package.
#'
#' @export
#' @param ... One or more calls that will evalulate to a logical.
#' @param env Environment with which to evaluate the calls.
#' @param msg Character string to use as the error message.
#' @return `TRUE` if all calls in `...` evaluate to `TRUE`. Otherwise an error
#'   condition is thrown.
#' @examples
#' assert(0L == 0L)
#' assert(is.character("foobar"), msg="This is not a character vector")
assert <- function(..., env=parent.frame(), msg=NULL) {
        stopifnot(is.null(msg) || (is.character(msg) && length(msg) == 1L))
        asserts <- eval(substitute(alist(...)))

        for (assertion in asserts) {
                res <- tryCatch({
                        eval(assertion, env)
                }, assertError=function(e) {
                        stop(e)
                })
                .validate_assert(res)

                if (!res) {
                        if (is.null(msg)) {
                                msg <- .get_message(assertion, env)
                        }
                        stop(.new_assertError(msg))
                }
        }

        TRUE
}

#' Assertion helpers
#'
#' Helper functions to test common assertions. All functions return a single
#' logical and have custom error messages.
#'
#' @name asshelpers
#' @param x Object with properties to query.
#' @param what Character string giving the attribute to check for.
#' @param class Character string giving the class name.
#' @param prog Character string giving the program to check for.
#' @return
#' `is_scalar()` checks if `x` has length of 1.
#'
#' `is_string()` checks if `x` is a character string.
#'
#' `is_flag()` checks if `x` is a logical vector of length 1.
#'
#' `is_integerish()` checks if `x` is an atomic vector of integer-like numbers.
#'
#' `is_number()` checks if `x` is a numeric vector of length 1.
#'
#' `is_natural_num()` checks if `x` is a single natural number (integer equal to
#' or greater than 0).
#' 
#' `is_function()` checks if `x` is a function.
#'
#' `has_attr()` checks if `x` has the attribute given by `what`.
#'
#' `inherits_from()` checks if `x` inherits from the class `class`.
#'
#' `in_path()` checks if `prog` is in PATH.
NULL

#' @rdname asshelpers
#' @export
is_scalar <- function(x) {
        length(x) == 1L
}
attr(is_scalar, "fail") <- function(call, env) {
        paste0(deparse(call$x), " is not a length-1 object")
}

#' @rdname asshelpers
#' @export
is_string <- function(x) {
        is.character(x) && length(x) == 1L
}
attr(is_string, "fail") <- function(call, env) {
        paste0(deparse(call$x), " is not a string (length-1 character vector)")
}

#' @rdname asshelpers
#' @export
is_flag <- function(x) {
        is.logical(x) && length(x) == 1L
}
attr(is_flag, "fail") <- function(call, env) {
        paste0(deparse(call$x), " is not a flag (length-1 logical vector)")
}

#' @rdname asshelpers
#' @export
is_integerish <- function(x) {
        if (!(typeof(x) %in% c("double", "integer"))) {
                return(FALSE)
        }

        if (!is.vector(x)) {
                return(FALSE)
        }

        y <- x[!is.na(x)]
        if (any(is.infinite(y))) {
                return(FALSE)
        }

        if (!all(y == trunc(y))) {
                return(FALSE)
        }

        TRUE
}
attr(is_integerish, "fail") <- function(call, env) {
        paste0(deparse(call$x), " is not integer-like")
}

#' @rdname asshelpers
#' @export
is_number <- function(x) {
        is.numeric(x) && length(x) == 1L
}
attr(is_number, "fail") <- function(call, env) {
        paste0(deparse(call$x), " is not a single number")
}

#' @rdname asshelpers
#' @export
is_natural_num <- function(x) {
        is_integerish(x) && length(x) == 1L && x >= 0L 
}
attr(is_natural_num, "fail") <- function(call, env) {
        paste0(deparse(call$x), " is not a natural number")
}

#' @rdname asshelpers
#' @export
is_function <- function(x) {
        is.function(x)
}
attr(is_function, "fail") <- function(call, env) {
        paste0(deparse(call$x), " is not a function")
}

#' @rdname asshelpers
#' @export
#' @return `TRUE` if `x` has the attribute `what`.
has_attr <- function(x, what) {
        !is.null(attr(x, what, exact=TRUE))
}
attr(has_attr, "fail") <- function(call, env) {
        paste0(
          deparse(call$x),
          " does not have attribute ",
          eval(call$what, env)
        )
}

#' @name asshelpers
#' @export
inherits_from <- function(x, class) {
         inherits(x, class)
}
attr(inherits_from, "fail") <- function(call, env) {
        paste0(
          deparse(call$x), " does not inherit from ", eval(call$class, env)
        )
}

#' @name asshelpers
#' @export
in_path <- function(prog) {
        is_string(prog) && Sys.which(prog) != ""
}
attr(in_path, "fail") <- function(call, env) {
        paste0(deparse(call$prog), " is not in $PATH")
}

.new_assertError <- function(message, call=NULL) {
        errorCondition(message, class="assertError", call=call)
}

.validate_assert <- function(res) {
        if (!is.logical(res)) {
                stop("assertion must return a logical value", call.=FALSE)
        } else if (any(is.na(res))) {
                stop("assertion must not contain missing values", call.=FALSE)
        } else if (length(res) != 1) {
                stop("assertion must have a length of 1", call.=FALSE)
        }

        TRUE
}

.get_message <- function(call, env=parent.frame()) {
        stopifnot(is.call(call), length(call) >= 1L)

        f <- eval(call[[1L]], env)
        if (!is.primitive(f)) {
                call <- match.call(f, call)
        }

        fail <- attr(f, "fail", exact=TRUE)
        if (!is.null(fail)) {
                msg <- fail(call, env)
                stopifnot(is.character(msg))
        } else {
                call_string <- deparse(call, width.cutoff=60L)
                if (length(call_string) > 1L) {
                        msg <- paste0(call_string[[1L]], "...")
                } else {
                        msg <- paste0(call_string, " is FALSE")
                }
        }

        msg
}
