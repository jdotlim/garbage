## General Utilities

#' Cleanup directories
#'
#' This function is meant to cleanup directories so it will only remove
#' directories that exist and not throw an error if any don't exist.
#'
#' @export
#' @param path Character vector of paths to delete.
#' @return Character vector of the deleted directories, invisibly.
cleanup_dirs <- function(path) {
        x <- path[fs::is_dir(path)]
        if (length(x) > 0L) {
                fs::dir_delete(x)
        }

        invisible(x)
}

#' Cleanup files
#'
#' This function is meant to cleanup files so it will only remove files
#' that exist and not throw an error if any don't exist.
#'
#' @export
#' @param path Character vector of paths to delete.
#' @return Character vector of the deleted files, invisibly.
cleanup_files <- function(path) {
        x <- path[fs::is_file(path)]
        if (length(x) > 0L) {
                fs::file_delete(x)
        }

        invisible(x)
}

#' Check if all numbers in a vector are equal
#'
#' The test for equality is done for the given tolerance.
#'
#' @export
#' @param x A numeric vector.
#' @param tol Double giving the tolerance to use when checking equality.
#' @return `TRUE` if all elements of `x` are equal within`tol`.
all_equal <- function(x, tol=.Machine$double.eps ^ 0.5) {
        assert(is.numeric(x))
        (max(x) - min(x)) < tol
}

#' Confirm that all files needed for an external program exist
#'
#' Many database search programs require static database files to run. This
#' function checks that such files exist and prints some helpful messages if
#' there are any missing files. It is currently not implemented as an assert
#' because generating and printing multi-line error condition messages doesn't
#' really work well. Can't get the initial "Error: " to not print.
#' @param path Character vector of required files.
#' @param what String naming what needs the files in `path`.
#' @return Invisibly return `TRUE` if all the files are present. If not all
#'   files are present, a message is generated listing the missing files and
#'   then a stop error condition is thrown.
#' @noRd
.ensure_prog_files <- function(path, what) {
        x <- fs::file_exists(path)
        if (all(x)) {
                return(invisible(TRUE))
        }
        
        message(
          sprintf("\"%s\" needed by `%s` is missing\n", path[!x], what)
        )
        stop(sprintf("Missing files required for `%s`", what), call.=FALSE)
}

#' 
